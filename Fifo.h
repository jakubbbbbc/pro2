#ifndef FIFO_H
#define FIFO_H

#include "Node.h"

class Fifo{
    private:
    Node* head;
    Node* tail;
    int listSize;

    public:

    Fifo();
    ~Fifo();
    void addNode();
    void getNode();
    void clearList();
    Fifo& operator + (Point&);
    Fifo& operator += (Point&);
    friend ostream& operator << (ostream& , Fifo);

};

#endif


pro2: main.o Menu.o Fifo.o Node.o Point.o
	g++ -g main.o Menu.o Fifo.o Node.o Point.o -o pro2

main.o: main.cpp Menu.h
	g++ -g -c main.cpp

Menu.o: Menu.cpp Menu.h
	g++ -g -c Menu.cpp

Fifo.o: Fifo.cpp Fifo.h
	g++ -g -c Fifo.cpp

Node.o: Node.cpp Node.h
	g++ -g -c Node.cpp

Point.o: Point.cpp Point.h
	g++ -g -c Point.cpp

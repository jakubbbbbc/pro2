#include "menu.h"
#include "Fifo.h"
#include<iostream>

using namespace std;

void mainMenu(){

    Fifo f;

    std::cout<<"Welcome to your Fifo list!";

    int choice =-1;
    while (choice !=0){
        std::cout<<"\n\nWhat would you like to do?\n\n";
        std::cout<<"1. show the line\n";
        std::cout<<"2. clear the line\n";
        std::cout<<"3. add element\n";
        std::cout<<"4. get element\n";
        std::cout<<"0. exit\n\n";

        std::cin>>choice;
        std::cout<<std::endl;

        switch(choice){
            case 0:
                f.clearList();
                break;
            case 1:
                std::cout<<f;
                break;
            case 2:
                f.clearList();
                std::cout<<"List cleared";
                break;
            case 3:
                f.addNode();
                break;
            case 4:
                f.getNode();
                break;
            default:
                break;
        }
    }

    std::cout<<"Have a nice day!\n";
}

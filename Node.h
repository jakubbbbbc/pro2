#ifndef NODE_H
#define NODE_H

#include "Point.h"

class Node{
    public:
    Point el;
    Node* next;

    //methods
    Node();
    Node(Point);
    ~Node();
};


#endif



#include "Point.h"

Point::Point(){}

Point::Point(int i){
    giveCoordinates();
}

Point::Point(double a, double b, double c){
    x=a;
    y=b;
    z=c;
}


Point::~Point(){}

Point Point::operator + (Point p2){
    Point temp;
    temp.x = x + p2.x;
    temp.y = y + p2.y;
    temp.z = z + p2.z;
    return temp;
}

void Point::operator += (Point p2){
    x = x + p2.x;
    y = y + p2.y;
    z = z + p2.z;
}

Point Point::operator - (Point p2){
    Point temp;
    temp.x = x - p2.x;
    temp.y = y - p2.y;
    temp.z = z - p2.z;
    return temp;
}

void Point::operator -= (Point p2){
    x = x - p2.x;
    y = y - p2.y;
    z = z - p2.z;
}

bool Point::operator == (Point p2){
    return (x==p2.x && y==p2.y && z==p2.z);
}

bool Point::operator != (Point p2){
    return (x!=p2.x || y!=p2.y || z!=p2.z);
}

ostream& operator << (ostream& os, Point p){
    os<<"("<<p.x<<", "<<p.y<<", "<<p.z<<")\n";
    return os;
}

void Point::giveCoordinates(){
    std::cout<<"Please specify x, y, and z:"<<std::endl;
    std::cout<<"x:   ";
    std::cin>>x;
    std::cout<<"y:   ";
    std::cin>>y;
    std::cout<<"z:   ";
    std::cin>>z;
}

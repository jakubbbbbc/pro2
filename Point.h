#ifndef POINT_H
#define POINT_H

#include<iostream>

using namespace std;

class Point{
    private:
    double x;
    double y;
    double z;

    public:
    Point();
    Point(int);
    Point(double, double, double);
    ~Point();
    Point operator + (Point);
    void operator += (Point);
    Point operator - (Point);
    void operator -= (Point);
    bool operator == (Point);
    bool operator != (Point);
    friend ostream& operator << (ostream& , Point);
    void giveCoordinates();

};

#endif
